""" Lisz main module implemented via the `Kivy framework <https://kivy.org>`__.

The node data implementations for this application is documented in the module :mod:`ae.lisz_app_data`.

TODO: create new docs for kivy_lisz and ae/aedev namespaces
TODO: remove `always_overscroll: False` from ScrollView in Main layout after issues #6952/#7196 get fixed.
TODO: fix/enhance android share intents and service startup (clean-up print() statements).
TODO: add feature to back-up/restore the config file (including all nodes) to/from a cloud storage (e.g. Google Drive).
TODO: keyboard-drag-drop ctr+D
TODO: drag&drop: auto-jump to destination(parent/sub) list in mouse-drag-drop
TODO: drag&drop: allow drop on first/last position of destination list
"""
# first import ae.base to runtime-wide-monkey-patch the python shutil package if running on android OS
from ae.base import load_dotenvs, os_platform                                                           # type: ignore

import ast
import os
from copy import deepcopy
from pprint import pformat
from typing import Any, Dict, List, Optional, Tuple, cast

from kivy.animation import Animation                                                                    # type: ignore
from kivy.app import App                                                                                # type: ignore
from kivy.clock import Clock                                                                            # type: ignore
from kivy.core.clipboard import Clipboard                                                               # type: ignore
from kivy.core.window import Window                                                                     # type: ignore
from kivy.factory import Factory                                                                        # type: ignore
from kivy.input import MotionEvent                                                                      # type: ignore
from kivy.metrics import sp                                                                             # type: ignore
from kivy.properties import (                                       # type: ignore # pylint: disable=no-name-in-module
    DictProperty, ListProperty, NumericProperty, ObjectProperty, BooleanProperty, StringProperty)
from kivy.uix.boxlayout import BoxLayout                                                                # type: ignore
from kivy.uix.widget import Widget                                                                      # type: ignore

from ae.files import read_file_text                                                                     # type: ignore
from ae.oaio_model import (                                                                             # type: ignore
    CREATE_ACCESS_RIGHT, DELETE_ACCESS_RIGHT, NO_ACCESS_RIGHT, READ_ACCESS_RIGHT, UPDATE_ACCESS_RIGHT)
from ae.core import start_app_service                                                                   # type: ignore
from ae.gui_app import APP_STATE_SECTION_NAME, id_of_flow, flow_action, flow_key                        # type: ignore
from ae.gui_help import TourBase, TourDropdownFromButton                                                # type: ignore
from ae.kivy.i18n import get_txt                                                                        # type: ignore
from ae.kivy.apps import KivyMainApp                                                                    # type: ignore
from ae.kivy.widgets import LOVE_VIBRATE_PATTERN, FlowDropDown, FlowPopup, FlowSelector, FlowToggler    # type: ignore

from ae.kivy.tours import AnimatedOnboardingTour                                                        # type: ignore
from ae.kivy_relief_canvas import relief_colors                                                         # type: ignore
from ae.kivy_sideloading import SideloadingMainAppMixin                                                 # type: ignore
from ae.lisz_app_data import (                                                                          # type: ignore
    FLOW_PATH_ROOT_ID, FOCUS_FLOW_PREFIX, flow_path_items_from_text, item_import_button_text,
    LiszItem, LiszNode, EditorOaioMixin, LiszDataMixin)


__version__ = '2.3.52'


load_dotenvs()

is_android = os_platform == 'android'

""" failed try-out using unicode characters as alternative to icon images

user_right_chars = {
    CREATE_ACCESS_RIGHT: '✻',   # U+273B, alternative: '🌟'=U+1F31F '🟎'=U+1F7CE
    DELETE_ACCESS_RIGHT: '🗑',  # U+1F5D1, alternative: '✗'=U+2717 '🗙'=U+1F5D9 '🗴
    UPDATE_ACCESS_RIGHT: '🖉',  # U+1F589
    READ_ACCESS_RIGHT: '👁',    # U+1F441, alternative: '🕮'=U+1F56E
    NO_ACCESS_RIGHT: '👽',      # U+1F47D, alternative: '📪'=U+1F4EA '🔒'=U+1F512 '🕆/🕇'=U+1F546/7 '🖐'=U+1F590
}

although they are shown correctly in the Pycharm editor, the kivy app displayed most of them as a box (either
crossed or with an question mark); only the char U+273B for CREATE got shown in the tried out fonts
(``DejaVuSans``, ``FreeSans``). setting font_name to ``noto`` or ``freefont`` on my Ubuntu machine did not even
load the font, but raised a ValueError: Couldn't open /etc/timidity.cfg: for font /usr/share/fonts/truetype/noto
"""


def selectable_and_total_sub_items(node: LiszNode, selected: int = 0, total: int = 0) -> Tuple[int, int]:
    """ determine the number of selectable and total leaf items of the node argument and their sub-nodes.

    :param node:                starting node.
    :param selected:            initial amount of selected items.
    :param total:               initial total amount of item leaves.
    :return:                    tuple of selectable and total leaf items.
                                dividing [0] by [1] results in a value between 0.0 and 1.0 reflecting the relation
                                between selected and unselected leaves.
    """
    for item in node:
        if 'node' in item:
            selected, total = selectable_and_total_sub_items(item['node'], selected, total)
        else:
            if item.get('sel'):
                selected += 1
            total += 1

    return selected, total


class DropPlaceholder(Widget):
    """ placeholder to display screen box to drop a dragged item onto. """
    destination = StringProperty()


class FlowPathJumperTour(TourDropdownFromButton):
    """ user preferences menu tour. """
    def __init__(self, main_app: 'LiszApp'):
        super().__init__(main_app)
        self.page_ids = [id_of_flow('open', 'flow_path_jumper'), TourDropdownFromButton.determine_page_ids]


class ItemDeletionConfirmPopup(FlowPopup):
    """ confirm dialog to delete a list leaf or node. """
    item_ids = ListProperty()
    item_node = ObjectProperty(allow_none=True)         # use ObjectProperty; ListProperty would make copy of LiszNode
    node_only = BooleanProperty()
    cut_to_clipboard = BooleanProperty()


class ItemEditor(FlowPopup, EditorOaioMixin):
    """ popup to edit the id, the user access rights of an item and to switch the item between node and leaf. """
    def __init__(self, **kwargs):
        self.item_id: str = kwargs.pop('item_id')                   #: id of the item to edit
        self.main_app = App.get_running_app().main_app
        self.item_data: LiszItem = self.main_app.item_by_id(self.item_id)

        self.init_oaio_vars()

        super().__init__(**kwargs)


class ItemSelConfirmPopup(FlowPopup):
    """ confirm dialog to de-/select the sub-leaves of a list node. """
    item_id = StringProperty()
    set_sel_to = BooleanProperty()


class ListFilterButton(FlowToggler):
    """ widget to toggle filter of selected/unselected list items. """
    name_part = StringProperty()


class NodeInfoPopup(FlowDropDown):
    """ dropdown to show node info. """
    node_info = DictProperty()


class NodeItem(BoxLayout):
    """ widget to display data item in list. """
    item_data: LiszItem = ObjectProperty()          # DictProperty would create new instance of our node item dict
    item_idx = NumericProperty()

    def __init__(self, **kwargs):
        self.item_data = kwargs.pop('item_data', {'id': ''})
        self.item_idx = kwargs.pop('item_idx', -1)

        if 'node' in self.item_data:
            node = self.item_data['node']
            sel, tot = selectable_and_total_sub_items(node)
            sel_factor = sel / (tot or 1.0)
            self.item_data['sel'] = sel_factor
        else:
            sel_factor = 0.0
        self.sel_majority_size_hint_x = max(sel_factor, 1 - sel_factor)
        self.sel_minority_size_hint_x = min(sel_factor, 1 - sel_factor)

        kivy_app = App.get_running_app()    # FrameworkApp
        self.app_root = kivy_app.root       # main view
        self.main_app = kivy_app.main_app   # kivy/console app
        self.space = int(self.main_app.font_size / 3.0)

        super().__init__(**kwargs)

        self.nic = kivy_app.root.ids.itemsContainer
        self.dragged_from_node = None
        self.dragging_to_parent_widget_instance = None
        self.dragging_to_parent_widget_idx = -1

    def on_touch_down(self, touch: MotionEvent) -> bool:
        """ move gliding list item widget. """
        if self.main_app.help_layout or not self.ids.dragHandle.collide_point(*touch.pos):
            return super().on_touch_down(touch)

        touch.grab(self)
        touch.ud[self] = 'drag'
        self.dragged_from_node = self.main_app.current_node_items
        self.main_app.dragging_item_idx = self.item_idx
        assert self.item_idx == self.dragged_from_node.index(self.item_data), \
            f"{self.item_idx} != {self.dragged_from_node.index(self.item_data)} {self.item_data=}"
        self.parent.remove_widget(self)
        self.x = touch.pos[0] - self.ids.dragHandle.x - self.ids.dragHandle.width / 2
        self.y = Window.mouse_pos[1] - self.height / 2
        self.app_root.add_widget(self)

        return True

    def on_touch_move(self, touch: MotionEvent) -> bool:
        """ move gliding list item widget. """
        if touch.grab_current is not self or touch.ud.get(self) != 'drag':
            return False

        self.pos = touch.pos[0] - self.ids.dragHandle.x - self.ids.dragHandle.width / 2, touch.pos[1] - self.height / 2

        app = self.main_app
        view = self.nic.parent
        if view.collide_point(*view.parent.to_local(*touch.pos)):
            placeholders = list(app.placeholders_above.values()) + list(app.placeholders_below.values())
            for child_idx, niw in enumerate(self.nic.children):
                ic_pos = self.nic.to_local(*view.to_local(*touch.pos))
                if niw not in placeholders and niw.collide_point(*ic_pos) \
                        and app.create_placeholder(child_idx, niw, ic_pos[1]):
                    self._restore_menu_bar()
                    return True

        menu_bar = self.app_root.ids.menuBar
        leave_button = menu_bar.ids.leaveItemButton     # .children[-3]
        if not leave_button.disabled and leave_button.collide_point(*menu_bar.to_local(*touch.pos)):
            app.cleanup_placeholder()
            if not self.dragging_to_parent_widget_instance:
                self.dragging_to_parent_widget_instance = leave_button
                self.dragging_to_parent_widget_idx = menu_bar.children.index(leave_button)
                menu_bar.remove_widget(leave_button)
                plh = Factory.DropPlaceholder(destination='leave',
                                              size_hint_x=None, size=(Window.width / 2, menu_bar.height))
                menu_bar.add_widget(plh, index=self.dragging_to_parent_widget_idx)
        elif touch.pos[1] < self.height:
            view.scroll_y = min(max(0, view.scroll_y - view.convert_distance_to_scroll(0, self.height)[1]), 1)
        elif touch.pos[1] > view.top - self.height:
            view.scroll_y = min(max(0, view.scroll_y + view.convert_distance_to_scroll(0, self.height)[1]), 1)

        return True

    def on_touch_up(self, touch: MotionEvent) -> bool:
        """ drop / finish drag. """
        if touch.grab_current is not self:
            return super().on_touch_up(touch)
        if touch.ud[self] != 'drag':
            return False

        app = self.main_app

        dst_node_path = None
        child_id = ''
        moved = False
        if self.dragging_to_parent_widget_instance or app.placeholders_above or app.placeholders_below:
            src_node = self.dragged_from_node
            assert src_node.index(self.item_data) == self.item_idx
            dst_item_id = ''
            if self.dragging_to_parent_widget_instance:
                dst_node_path = app.flow_path[:-1]
                dst_item_id = flow_key(app.flow_path[-1])
            elif app.placeholders_above and app.placeholders_below:   # drop into node
                item_idx = list(app.placeholders_below.keys())[0]
                child_id = src_node[item_idx]['id']
                dst_node_path = app.flow_path + [id_of_flow('enter', 'item', child_id), ]
            else:
                if app.placeholders_above:
                    item_idx = list(app.placeholders_above.keys())[0]
                else:
                    item_idx = list(app.placeholders_below.keys())[0] + 1
                dst_item_id = src_node[item_idx]['id'] if item_idx < len(src_node) else ''
            moved = app.move_item(src_node, self.item_data['id'], dropped_path=dst_node_path, dropped_id=dst_item_id)

        self._restore_menu_bar()
        self.app_root.remove_widget(self)
        self.nic.add_widget(self, index=len(self.nic.children) - self.item_idx)
        app.cleanup_placeholder()

        app.dragging_item_idx = None
        self.dragged_from_node = None
        touch.ungrab(self)

        # full refresh or only set/change focus; on_flow_id() with 'focus' flow action does also a full refresh/redraw
        if moved:
            app.play_sound('edited')
            app.save_app_states()
            app.refresh_node_widgets()
        else:
            flow_id = id_of_flow('focus', 'item', child_id or self.item_data['id'])
            if flow_id == app.flow_id:
                # remove focus from focused item - allowing clipboard item copies of current node (not current item)
                app.change_app_state('flow_id', id_of_flow(''))      # using instead change_flow() recovers last focus
            else:
                app.change_flow(flow_id)

        return True

    def _restore_menu_bar(self):
        if self.dragging_to_parent_widget_instance:
            menu_bar = self.app_root.ids.menuBar
            leaf_button = self.dragging_to_parent_widget_instance
            place_holder = menu_bar.children[self.dragging_to_parent_widget_idx]
            menu_bar.remove_widget(place_holder)
            menu_bar.add_widget(leaf_button, index=self.dragging_to_parent_widget_idx)
            # not needed: self.dragging_to_parent_widget_idx = -1
            self.dragging_to_parent_widget_instance = None


class LiszOnboardingTour(AnimatedOnboardingTour):
    """ overridden to add app-specific tour pages including animations and shaders. """
    _tour_page_id_prefix = "lisz_"  # page id prefix of additional app-specific tour pages

    def __init__(self, main_app: 'LiszApp'):
        super().__init__(main_app)

        self._demo_item_id = get_txt("demo item")
        self._demo_sub_item_id1 = get_txt("demo sub item 1")
        self._demo_sub_item_id2 = get_txt("demo sub item 2")
        self._demo_sub_list_id = get_txt("demo sub-list")

        pip = LiszOnboardingTour._tour_page_id_prefix
        insert_idx = self.page_ids.index('tour_end')
        self.page_ids[insert_idx:insert_idx] = [
            pip + "1empty",
            pip + "2add_item", pip + "2enter_id", pip + "2entered_id",
            pip + "3enter_id", pip + "3toggle_list", pip + "3u_added_list",
            pip + "4navigate_down", pip + "4navigate_up",
            pip + "5navigate"]

        matchers = self.pages_explained_matchers
        matchers[pip + "2add_item"] = id_of_flow('add', 'item')
        matchers[pip + "2enter_id"] = id_of_flow('change', 'item_id')
        matchers[pip + "2entered_id"] = lambda _w: hasattr(_w, 'majority_sel')   # or item_data/item_idx
        matchers[pip + "3enter_id"] = id_of_flow('change', 'item_id')
        matchers[pip + "3toggle_list"] = id_of_flow('toggle', 'node_or_item')
        matchers[pip + "3u_added_list"] = (lambda _w: getattr(_w, 'item_data', {}).get('id') == self._demo_item_id,
                                           lambda _w: getattr(_w, 'item_data', {}).get('id') == self._demo_sub_list_id,)
        matchers[pip + "4navigate_down"] = id_of_flow('enter', 'item', self._demo_sub_list_id)
        matchers[pip + "4navigate_up"] = id_of_flow('leave', 'item')
        matchers[pip + "5navigate"] = id_of_flow('open', 'flow_path_jumper')

        ans = self.pages_animations
        ans[pip + "2add_item"] = (
            ('@',
             "A(height=layout.explained_size[1] * 1.2, t='in_out_sine', d=1.5) + "
             "A(height=layout.explained_size[1] / 1.2, t='in_out_sine', d=3.0) + "
             "A(height=layout.explained_size[1], t='in_out_sine', d=1.5)"), )
        ans[pip + "3toggle_list"] = (('@tap_pointer', "tour.tap_animation()"), )

        sha = self.pages_shaders
        sha[pip + "2add_item"] = (
            ('layout', {'add_to': 'before', 'alpha': "lambda: layout.ani_value / 3.0",
                        'center_pos': "lambda: list(map(float, layout.explained_widget.center))",
                        'shader_code': "=plunge_waves", 'time': "lambda: -Clock.get_boottime()",
                        'tint_ink': [0.21, 0.39, 0.09, 0.9]}),
        )

        ans = self.switch_next_animations
        ans[pip + "2add_item"] = (('tap_pointer', "tour.tap_animation()"), )
        ans[pip + "2enter_id"] = (('tap_pointer', "tour.tap_animation('tooltip')"), )
        ans[pip + "3enter_id"] = (('tap_pointer',  "tour.tap_animation(':' + id_of_flow('toggle', 'node_or_item'))"), )
        ans[pip + "4navigate_down"] = (('tap_pointer', "tour.tap_animation()"), )
        ans[pip + "4navigate_up"] = (('tap_pointer', "tour.tap_animation()"), )

    def setup_app_flow(self):
        """ setup app for each tour page. """
        super().setup_app_flow()

        main_app = self.main_app
        pip = LiszOnboardingTour._tour_page_id_prefix
        page_id = self.page_ids[self.page_idx]
        if not page_id.startswith(pip):
            main_app.refresh_all()
            return

        root_node = []
        if page_id >= pip + "2entered_id":
            root_node.append({'id': self._demo_item_id})
        if page_id >= pip + "3toggle_list":
            root_node.append({'id': self._demo_sub_list_id, 'node': [
                {'id': self._demo_sub_item_id1},
                {'id': self._demo_sub_item_id2}]})
        main_app.change_app_state('root_node', root_node)

        flow_id = id_of_flow('')
        if page_id in (pip + "2enter_id", pip + "3enter_id"):
            flow_id = id_of_flow('add', 'item')
        elif page_id == pip + "3toggle_list":
            flow_id = id_of_flow('edit', 'item', self._demo_sub_list_id)
        elif page_id == pip + "4navigate_up":
            flow_id = id_of_flow('enter', 'item', self._demo_sub_list_id)
        main_app.change_app_state('flow_path', [])
        main_app.refresh_all()      # setup shadow/layout list, focus and flow id to prepare flow change
        main_app.change_flow(flow_id)
        main_app.refresh_all()      # update screen from flow change (e.g. flow path jumper text)

    def setup_layout(self):
        """ overridden to simulate user input in an explained text input widget. """
        super().setup_layout()

        pip = LiszOnboardingTour._tour_page_id_prefix
        page_id = self.page_ids[self.page_idx]
        text_to_enter = ''

        if page_id == pip + "2enter_id":
            text_to_enter = self._demo_item_id
        elif page_id == pip + "3enter_id":
            text_to_enter = self._demo_sub_list_id

        if text_to_enter:
            text_input = self.top_popup.ids.itemTextInp
            self.simulate_text_input(text_input, text_to_enter)
            text_input.focus = False
            text_input.hide_keyboard()

    def teardown_app_flow(self):
        """ overridden to refresh app screen on tour stop/page-reset. """
        pip = LiszOnboardingTour._tour_page_id_prefix
        page_id = self.page_ids[self.page_idx]
        if self.top_popup and page_id in (pip + "2enter_id", pip + "3enter_id", pip + "3toggle_list"):
            self.top_popup.close()

        super().teardown_app_flow()  # main_app.refresh_all() gets called via main_app.on_tour_exit() on tour stop/end


class UserAccessRightPopup(FlowSelector):
    """ show a menu with the possible access rights for a user (specified by text). """
    def __init__(self, **kwargs):
        editor = kwargs.pop('editor')
        username = kwargs.pop('usr_name')
        access_right = kwargs.pop('acc_right')
        userz_popup = kwargs.pop('parent_popup_to_close')

        main_app = App.get_running_app().main_app
        image_size = (round(main_app.font_size * 1.35), ) * 2
        ink = main_app.update_ink

        right_options = [DELETE_ACCESS_RIGHT, UPDATE_ACCESS_RIGHT, READ_ACCESS_RIGHT, NO_ACCESS_RIGHT]
        right_options.remove(access_right)
        kwargs['child_data_maps'] = [
            {
                'cls': 'UserAccessSelectButton',
                'kwargs': {
                 'text': get_txt(f"access_right_{right_opt}"),
                 'tap_flow_id': id_of_flow('change', 'user_access', right_opt),
                 'tap_kwargs': {'editor': editor, 'user_id': username, 'new_right': right_opt,
                                'popups_to_close': (userz_popup, 'replace_with_data_map_popup', )},
                 'icon_name': f'access_right_{right_opt}',
                 'image_offset': (sp(3), ) * 2,
                 'image_size': image_size,
                 'size_hint_x': None, 'width': 222,
                 'square_fill_ink': ink,
                 # 'ellipse_fill_size': image_size,
                 'relief_square_outer_colors': relief_colors(ink),
                 'relief_ellipse_outer_lines': round(sp(9)),
                 },
            } for right_opt in right_options
        ]

        super().__init__(**kwargs)


class UserzAccessPopup(FlowDropDown):
    """ show a list of all userz with their access right for an oai object, opened from ItemEditor. """
    def __init__(self, **kwargs):
        editor: ItemEditor = kwargs.pop('editor')  # providing attributes item_id, oaio_id and method userz_access_right

        main_app = App.get_running_app().main_app

        image_size = (round(main_app.font_size * 1.35), ) * 2
        image_offset = (sp(6), ) * 2

        menu_items = []
        registered = editor.oaio_is_registered
        if not registered:
            menu_items.append({
                # 'cls': 'FlowButton',
                'kwargs': {
                    'text': get_txt('register for you only'),
                    'tap_flow_id': id_of_flow('change', 'user_access'),
                    'tap_kwargs': {'editor': editor, 'user_id': main_app.user_id, 'new_right': CREATE_ACCESS_RIGHT,
                                   'popups_to_close': ('replace_with_data_map_popup',)},
                    'icon_name': 'open_userz_access',
                    'image_offset': image_offset,
                    'image_size': image_size,
                }
            })
            menu_items.append({
                # 'cls': 'FlowButton',
                'kwargs': {
                    'text': get_txt('register and grant read access to all users'),
                    'tap_flow_id': id_of_flow('change', 'user_access'),
                    'tap_kwargs': {'editor': editor, 'user_id': main_app.user_id,
                                   'new_right': CREATE_ACCESS_RIGHT, 'others_right': READ_ACCESS_RIGHT,
                                   'popups_to_close': ('replace_with_data_map_popup', )},
                    'icon_name': 'open_userz_access',
                    'image_offset': image_offset,
                    'image_size': image_size,
                }
            })

        elif editor.user_can_unregister:
            menu_items.append({
                # 'cls': 'FlowButton',
                'kwargs': {
                    'text': get_txt('unregister and revoke read access from all users'),
                    'tap_flow_id': id_of_flow('change', 'user_access'),
                    'tap_kwargs': {'editor': editor, 'user_id': main_app.user_id,
                                   'new_right': NO_ACCESS_RIGHT, 'others_right': NO_ACCESS_RIGHT,
                                   'popups_to_close': ('replace_with_data_map_popup',)},
                    'icon_name': 'access_right_x',
                    'image_offset': image_offset,
                    'image_size': image_size,
                }
            })

        rights_editable = not registered or editor.user_is_creator

        if menu_items and rights_editable:
            menu_items.append({
                'cls': 'ImageLabel',
                'kwargs': {'text': get_txt('------------------------------------'), }
            })

        for usr_acc_right in (editor.userz_access_rights if rights_editable else ()):
            usr_name = usr_acc_right['username']
            if usr_name == main_app.user_id:
                continue
            acc_right = usr_acc_right['access_right']

            menu_items.append(
                {   # 'cls': 'FlowButton',
                    'kwargs': {
                        'text': usr_name,
                        'tap_flow_id': id_of_flow('open', 'user_access_right', usr_name),
                        'tap_kwargs': {'popup_kwargs': {'editor': editor, 'usr_name': usr_name, 'acc_right': acc_right,
                                                        'parent_popup_to_close': self, 'text': usr_name}},
                        'icon_name': f'access_right_{acc_right}',
                        'image_offset': image_offset,
                        'image_size': image_size,
                    }
                }
            )

        kwargs['child_data_maps'] = menu_items

        super().__init__(**kwargs)


class LiszApp(SideloadingMainAppMixin, LiszDataMixin, KivyMainApp):
    """ main app class. """
    dragging_item_idx: Optional[int] = None             #: index of dragged data if in drag mode else None
    file_chooser_paths: List[str] = []                  #: store recently used paths as app state for file chooser
    items_container: Optional[Widget]                   #: list container widget shortcut
    menu_bar_ids: Any                                   #: menu bar ids shortcut (is actually a DictProperty)
    placeholders_above: Dict[int, Widget] = {}          #: added placeholder above widgets (used for drag+drop)
    placeholders_below: Dict[int, Widget] = {}          #: added placeholder below widgets (used for drag+drop)
    refreshing_widgets: bool = False                    #: True only in refresh call (performance opt)

    def _init_default_user_cfg_vars(self):
        super()._init_default_user_cfg_vars()
        self.user_specific_cfg_vars |= {
            (APP_STATE_SECTION_NAME, 'filter_selected'),
            (APP_STATE_SECTION_NAME, 'filter_unselected'),
            (APP_STATE_SECTION_NAME, 'root_node'),
        }

    # ******  public methods  *******************************************************************************

    def create_item_widgets(self, item_idx: int, nid: LiszItem) -> List[Widget]:
        """ create widgets to display one item, optionally with placeholder markers.

        :param item_idx:        index of item_data within current list.
        :param nid:             list item data.
        :return:                list of created widgets: one NodeItem widget with item_data from nid and optional
                                placeholders above/below.
        """
        widgets = []

        if item_idx in self.placeholders_above:
            widgets.append(self.placeholders_above[item_idx])

        niw = Factory.NodeItem(item_data=nid, item_idx=item_idx)
        widgets.append(niw)
        assert niw.item_data is nid
        assert niw.item_idx == item_idx

        if item_idx in self.placeholders_below:
            widgets.append(self.placeholders_below[item_idx])

        return widgets

    def create_placeholder(self, child_idx: int, niw: Widget, touch_y: float) -> bool:
        """ create placeholder data structures. """
        child_idx -= self.cleanup_placeholder(child_idx=child_idx)
        part = (touch_y - niw.y) / niw.height
        item_idx = niw.item_idx
        self.vpo(f"LiszApp.create placeholder {child_idx:2} {item_idx:2} {niw.item_data['id'][:9]:9}"
                 f" {niw.y:4.2f} {touch_y:4.2f} {part:4.2f}")
        if 'node' in niw.item_data and 0.123 < part < 0.9:
            self.placeholders_above[item_idx] = Factory.DropPlaceholder(destination='enter', height=niw.height / 2.7)
            self.placeholders_below[item_idx] = Factory.DropPlaceholder(destination='enter', height=niw.height / 2.7)
            self.play_vibrate((0.03, 0.12, 0.09, 0.12, 0.09, 0.12))
        elif -0.111 < part < 1.11:
            placeholders = self.placeholders_above if part >= 0.501 else self.placeholders_below
            placeholders[item_idx] = Factory.DropPlaceholder(destination='current', height=niw.height * 1.11)
            self.play_vibrate((0.03, 0.09, 0.09, 0.09))
        else:
            return False

        # add widgets without redrawing (self.on_flow_id())
        nic = cast(Widget, self.items_container)
        for phw in self.placeholders_below.values():
            nic.add_widget(phw, index=child_idx)
            nic.height += phw.height
            child_idx += 1
        for phw in self.placeholders_above.values():
            nic.add_widget(phw, index=child_idx + 1)
            nic.height += phw.height

        return True

    def cleanup_placeholder(self, child_idx: int = 0) -> int:
        """ cleanup placeholder data and widgets. """
        delta_idx = 0
        placeholders = list(self.placeholders_above.values()) + list(self.placeholders_below.values())
        for placeholder in placeholders:
            nic = placeholder.parent
            if nic:
                del_idx = nic.children.index(placeholder)
                if del_idx < child_idx:
                    delta_idx = 1
                nic.remove_widget(placeholder)
                nic.height -= placeholder.height

        self.placeholders_above.clear()
        self.placeholders_below.clear()

        return delta_idx

    def edit_item_finished(self, editor: ItemEditor, new_id: str, want_node: bool = False):
        """ finished list add/edit callback from on_dismiss event of ItemEditor.

        :param editor:          ItemEditor instance.
        :param new_id:          new item id.
        :param want_node:       pass True for create/convert item to node.
        """
        old_id = editor.item_id
        self.vpo(f"LiszApp.edit_item_finished {old_id=} {new_id=} {want_node=} {editor.close_with_cancel=}")
        if editor.close_with_cancel:
            self.play_sound('cancel')
        else:
            self.change_flow(id_of_flow('save', 'item', old_id), new_id=new_id, want_node=want_node, editor=editor)

    def export_node(self, flow_path: List[str], file_path: str = "", node: Optional[LiszNode] = None) -> str:
        """ overwritten export node for overwrite file path default and to log call and error.

        :param flow_path:       flow path of the node to export (def=OS documents folder / app name).
        :param file_path:       path to the folder wherein to store the node data (def=OS documents folder + app name).
        :param node:            explicit/filtered node items (if not passed then all items will be exported).
        :return:                `None` if node got exported without errors, else the raised exception.
        """
        if not file_path:
            file_path = self.documents_root_path
        self.vpo(f"LiszApp.export_node({flow_path}, {file_path})")

        err_msg = super().export_node(flow_path, file_path=file_path, node=node)
        if err_msg:
            self.po(f"LiszApp.export_node({flow_path}, {file_path}) exception: {err_msg}")
            self.show_message(err_msg, title=get_txt("export error"))

        return err_msg

    def on_app_build(self):
        """ gui app pre-build callback/event to override the onboarding tour class and to prepare android intents. """
        super().on_app_build()
        if is_android:
            prepare_android_intent_handlers("KIVY APP BUILD")

    def on_app_built(self):
        """ callback after app build to establish shortcuts to most referenced widget and widget ids. """
        super().on_app_built()
        root_ids = self.framework_root.ids
        self.menu_bar_ids = root_ids.menuBar.ids
        self.items_container = root_ids.itemsContainer

    def on_app_resume(self):
        """ kivy app resume event. """
        super().on_app_resume()
        print("LiszApp RESUME", self)

    def on_app_run(self):
        """ kivy app run event. """
        super().on_app_run()
        self.refresh_current_node_items_from_flow_path()
        if self.debug or is_android:
            Clock.schedule_interval(self.share_data_check, self.get_variable('poll_interval', default_value=6.9))

    def on_app_started(self):
        """ callback after app built to draw/refresh gui (on_app_build()/on_app_built() would be to early). """
        self.vpo("LiszApp.on_app_started event handler called")
        super().on_app_started()
        self.refresh_all()
        self.refresh_all()  # 2nd call to have scrollview content on top, with always_overscroll=True even further down

    def on_clipboard_key_c(self, lit: str = ""):
        """ copy focused item or the currently displayed node items to the OS clipboard.

        :param lit:             string literal to copy (def=current_item_or_node_literal()).
        """
        if not lit:
            lit = self.current_item_or_node_literal()
        self.vpo(f"LiszApp.on_clipboard_key_c: copying {lit}")
        Clipboard.copy(lit)

    def on_clipboard_key_v(self):
        """ paste copied item or all items of the current node from the OS clipboard into the current node. """
        lit = Clipboard.paste()
        self.vpo(f"LiszApp.on_clipboard_key_v: pasting {lit}")
        err_msg, flow_path_text, items = flow_path_items_from_text(lit)
        node = None
        if flow_path_text:
            flow_path = self.flow_path_from_text(flow_path_text, skip_check=True)
            node = self.flow_path_node(flow_path, create=True)
        err_msg += self.add_items(items, node_to_add_to=node)
        if err_msg:
            self.show_message(err_msg, title=get_txt("{count} error(s) in adding {len(items)} item(s)",
                                                     count=err_msg.count('\n') + 1))
        self.refresh_node_widgets()

    def on_clipboard_key_x(self):
        """ cut focused item or the currently displayed node items to the OS clipboard. """
        lit = self.current_item_or_node_literal()
        self.vpo(f"LiszApp.on_clipboard_key_x: cutting {lit}")
        Clipboard.copy(lit)
        if lit.startswith('['):
            self.delete_items(*[item['id'] for item in self.current_node_items])
        elif lit.startswith('{'):
            self.delete_items(flow_key(self.flow_id))
        else:
            self.delete_items(lit)
        self.refresh_node_widgets()

    def on_item_add(self, _item_id: str, _event_kwargs: Dict[str, Any]) -> bool:
        """ start/initiate the addition of a new list item. """
        self.vpo(f"LiszApp.on_item_add({_item_id} {_event_kwargs})")

        pop_height = self.font_size * 2.1
        pup = ItemEditor(item_id='',        # differentiate between addition (=='') and edition (==id to edit)
                         title=get_txt("create new item"),
                         pos_hint={'x': 0, 'y': (
                            cast(Widget, self.items_container).parent.top - pop_height) / Window.height},
                         size_hint=(None, None),
                         size=(Window.width, pop_height),
                         background_color=self.update_ink,
                         separator_height=0,
                         )
        pup.open()          # popup will call back self.edit_item_finished on dismiss/close

        return True

    def on_item_edit(self, item_id: str, _event_kwargs: Dict[str, Any]) -> bool:
        """ edit list item. """
        self.vpo(f"LiszApp.on_item_edit({item_id} {_event_kwargs})")

        item_id = flow_key(self.flow_id) or item_id     # `or item_id` needed for app tour page "3toggle_list"
        niw = cast(Widget, self.widget_by_id(item_id))
        svw = cast(Widget, self.items_container).parent
        svw.scroll_to(niw, animate=False)
        pos = niw.to_window(*niw.pos)
        pup = ItemEditor(item_id=niw.item_data['id'],  # used by kv-rules and help texts to get old/ori item id
                         title=get_txt("edit item {niw.item_data['id']}"),
                         pos_hint={'x': pos[0] / Window.width,
                                   'y': min(max(0, pos[1]), svw.top - niw.height) / Window.height},
                         size_hint=(None, None),
                         size=(svw.width, self.font_size * 2.1),
                         background_color=self.update_ink,
                         )
        pup.open()                                   # popup will call edit_item_finished() on dismiss/close

        return True

    def on_item_enter(self, item_id: str, event_kwargs: Dict[str, Any]) -> bool:
        """ animate entering node. """
        nic = cast(Widget, self.items_container)
        ani = Animation(x=Window.width, d=0.003) + Animation(x=nic.x, d=0.21, t='out_quint')
        ani.start(nic)
        return super().on_item_enter(item_id, event_kwargs)

    def on_item_leave(self, item_id: str, event_kwargs: Dict[str, Any]) -> bool:
        """ animate leaving of node. """
        nic = cast(Widget, self.items_container)
        ani = Animation(y=100, right=100, d=0.006) + Animation(y=nic.y, right=nic.right, d=0.21, t='out_quint')
        ani.start(nic)
        return super().on_item_leave(item_id, event_kwargs)

    def on_item_save(self, item_id: str, event_kwargs: Dict[str, Any]) -> bool:
        """ update item specified by flow key using the new values/changes given in event_kwargs. """
        self.vpo(f"LiszApp.on_item_save('{item_id}', {event_kwargs})")

        adding_item = (item_id == '')
        item_idx = -1 if adding_item else self.find_item_index(item_id)

        action_or_err = self.edit_validate(item_idx, **event_kwargs)

        if action_or_err:
            if action_or_err.startswith('request_delete_confirmation'):
                self.change_flow(id_of_flow('confirm', 'item_deletion', item_id),
                                 popup_kwargs={'node_only': action_or_err.endswith('_for_node')})
            else:
                self.show_message(action_or_err,
                                  title=get_txt("error on adding new item '{item_id}'" if adding_item else
                                                "error on editing item '{item_id}'"))

            return False

        if adding_item:  # new list item added (with non-empty new_id) at index 0 of the current node
            item_idx = 0

        self.play_vibrate(LOVE_VIBRATE_PATTERN)
        if self.current_node_items:  # set next flow id if current node not empty (node was empty before and add cancel)
            event_kwargs['flow_id'] = id_of_flow('focus', 'item', self.current_node_items[item_idx]['id'])
        event_kwargs['changed_event_name'] = 'refresh_all'

        self.save_app_states()
        return True

    def on_items_delete(self, item_ids: str, event_kwargs: Dict[str, Any]) -> bool:
        """ delete the items or sub-nodes of the items specified by :paramref:`~on_items_delete.item_ids`.

        :param item_ids:        list/tuple literal of item ids of the items to delete (or the nodes of them).
        :param event_kwargs:    If contains non-empty 'parent_node' key then the items specified by the
                                :paramref:`~on_items_delete.item_ids` argument will be deleted from the
                                passed node instead of the current node.
                                If contains 'node_only' key with a True value then delete only the nodes of the items.
                                If contains 'cut_to_clipboard' with True value then copy the deleted items to clipboard.
        :return:                True to process/change flow id.
        """
        self.vpo(f"LiszApp.on_items_delete({item_ids} {event_kwargs})")

        item_ids = ast.literal_eval(item_ids)
        assert item_ids and isinstance(item_ids, (list, tuple))

        node_only = event_kwargs.get('node_only', False)
        del_items = self.delete_items(*item_ids, parent_node=event_kwargs.get('parent_node'), node_only=node_only)
        if event_kwargs.get('cut_to_clipboard', False):
            self.on_clipboard_key_c(pformat(del_items))
        if not node_only:
            self.change_flow(id_of_flow(''), reset_last_focus_flow_id=True)

        self.play_sound('deleted')
        self.refresh_all()
        return True

    def on_node_extract(self, flow_path_text: str, event_kwargs: dict) -> bool:
        """ DEBUG """
        ret = super().on_node_extract(flow_path_text, event_kwargs)
        if event_kwargs['extract_type'].startswith('share'):
            print("DELAYED call of share_node requested")
        return ret

    def on_node_import(self, _flow_key: str, event_kwargs: Dict[str, Any]) -> bool:
        """ import single node items to the node and index specified via the event_kwargs items. """
        node = event_kwargs['node_to_import']
        add_node_id = event_kwargs.get('add_node_id', '')
        parent_index = event_kwargs.get('import_items_index', 0)
        parent_node = event_kwargs.get('import_items_node', self.current_node_items)
        self.vpo(f"LiszApp.on_import_node({node=}, {add_node_id=}, {parent_node=}, {parent_index=})")

        if add_node_id:
            err_msg = self.import_node(add_node_id, node, parent=parent_node, item_index=parent_index)
        else:
            err_msg = self.import_items(node, parent=parent_node, item_index=parent_index)
        if err_msg:
            self.po(f"LiszApp.on_import_node error: {err_msg}")
            self.show_message(err_msg, title=get_txt("import error(s)", count=err_msg.count("\n") + 1))

        self.save_app_states()

        if parent_node == self.current_node_items:
            self.refresh_node_widgets()

        return True

    def on_node_jump(self, flow_path_text: str, event_kwargs: Dict[str, Any]) -> bool:
        """ overwrite to ensure close of dropdown. """
        accepted = super().on_node_jump(flow_path_text, event_kwargs)
        drop_downs = list(self.popups_opened(classes=(Factory.FlowPathJumperPopup, )))
        if drop_downs:
            drop_downs[0].dismiss()        # .close() node jumper dropdown
        return accepted

    def on_node_paste(self, _flow_key: str, _event_kwargs: Dict[str, Any]) -> bool:
        """ flow change event handler called from the alt_add popup menu. """
        self.on_clipboard_key_v()
        return True

    def on_tour_exit(self, _tour_instance: TourBase):
        """ reset/refresh app on stop/end of any tour.

        :param _tour_instance:  instance of the ended tour.
        """
        self.refresh_all()

    def refresh_node_widgets(self):
        """ refresh displayed node item widgets and mark current list item of filtered current node data. """
        assert not self.refreshing_widgets
        self.refreshing_widgets = True
        try:
            flow_id = self.flow_id
            self.vpo(f"LiszApp.refresh_node_widgets: {flow_id=} {self.flow_path=}")
            self.filtered_indexes = []

            lf_ds = self.menu_bar_ids.listFilterSelected.state == 'normal'
            lf_ns = self.menu_bar_ids.listFilterUnselected.state == 'normal'
            nic = self.items_container
            nic.clear_widgets()
            height = 0
            for item_idx, nid in enumerate(self.current_node_items):
                if item_idx != self.dragging_item_idx:
                    widgets = self.create_item_widgets(item_idx, nid)   # has to be created to re-calc sel for nodes
                    sel_state = nid.get('sel')
                    if lf_ds and sel_state or lf_ns and sel_state != 1.0:
                        for niw in widgets:
                            nic.add_widget(niw)
                            height += niw.height
                            self.filtered_indexes.append(item_idx)
            nic.height = height

            # ensure that current leaf/node is visible - if still exists in current list
            if flow_action(flow_id) == 'focus':
                niw = self.widget_by_id(flow_key(flow_id))
                if niw:
                    nic.parent.scroll_to(niw, animate=False)
        finally:
            assert self.refreshing_widgets
            self.refreshing_widgets = False

    def share_data_check(self, *_args):
        """ timer callback to check if we received data via ACTION_VIEW/share intent. """
        print(" >>>> SHARE_DATA_CHECK TIMER - check if exists shared text file", shared_text_file_path)
        if os.path.exists(shared_text_file_path):
            shared_text = read_file_text(shared_text_file_path)
            print("      OK IT EXISTS shared text length=", len(shared_text))
            err_msg, _flow_path, items = self.shared_data_received(shared_text)
            if err_msg:
                print("      SHARED TEXT ADDED WITH ERROR", err_msg)
            self.save_app_states()
            print("      DELETE SHARE TEXT FILE", shared_text_file_path)
            os.remove(shared_text_file_path)
            self.refresh_all()
            if items:
                self.show_message(get_txt("added to {flow_path} the items: {items}\nERRORS={err_msg}"),
                                  title=get_txt("received shared items"))

    def shared_data_received(self, shared_text: str) -> Tuple[str, List[str], LiszNode]:
        """ process shared text sent via share intent to this app.

        :param shared_text:     shared text received from ACTION_VIEW/share intent.
        :return:                tuple of error message (or empty string if no error occurred), flow path and node.
        """
        err_msg, flow_path_text, items = flow_path_items_from_text(shared_text)
        if err_msg:
            return err_msg, [], []

        flow_path = self.flow_path_from_text(flow_path_text, skip_check=True)
        node_to_add_to = self.flow_path_node(flow_path, create=True)
        return self.add_items(items, node_to_add_to=node_to_add_to), flow_path, items

    def share_node(self, flow_path: List[str], node: Optional[LiszNode] = None):
        """ share node as android action send intent.

        (called also in other platforms; exceptions caught because called by self.on_node_extract via self.call_method).

        :param flow_path:       flow path of node (parts) to share.
        :param node:            node to share (default=the node specified by flow_path).
        """
        share_name = self.flow_path_text(flow_path, min_len=0)
        if node is None:
            node = self.flow_path_node(flow_path)
        share_plain_text(share_name + "\n" + pformat(node), share_name)

    def show_add_options(self, opener: Widget) -> bool:
        """ open context menu with all available actions to add/import new node/item(s).

        :param opener:          FlowButton/widget that initiated the opening this context menu.
        :return:                True if any add options are available and got displayed else False.
        """
        def _add_importable_menus(_from: str, _importable_and_errors_node: LiszNode):
            def _add_mnu_but(_chk: bool, _txt: str, _idx: int):
                if _chk:
                    mnu_but: dict[str, Any] = deepcopy(def_but)
                    mnu_but['kwargs']['text'] = _txt
                    mnu_but['kwargs']['tap_kwargs']['import_items_index'] = _idx
                    mnu_buts.append(mnu_but)

            mnu_maps: list[dict] = []
            for item in _importable_and_errors_node:
                if mnu_maps:                      # add separator widget into de child maps of the menu dropdown
                    mnu_maps.append({'cls': 'Widget', 'kwargs': {'size_hint_y': None, 'height': self.font_size / 1.8}})

                mnu_buts: list[dict] = []
                if '_err_' not in item:
                    node_id, node = item['id'], item['node']
                    what = () if self.verbose else ('names', )
                    info = self.node_info(node, what=what)
                    if len(node) > 1:
                        text = get_txt("{info['selected_leaf_count'] + info['unselected_leaf_count']} items")
                    else:
                        text = get_txt("item") + f" '{node[0]['id']}'"
                    if node_id:
                        info['name'] = node_id
                        text = get_txt("node '{node_id}' with ") + text
                    inf_but = {'cls': 'ImportInfoButton',
                               'kwargs': {'text': text,
                                          'tap_flow_id': id_of_flow('open', 'node_info'),
                                          'tap_kwargs': {'popup_kwargs': {'node_info': info}}, }}
                    def_but = {'cls': 'IconButton',
                               'kwargs': {'text': get_txt("current list begin"),
                                          'tap_flow_id': id_of_flow('import', 'node'),
                                          'tap_kwargs': {'node_to_import': node,
                                                         'add_node_id': node_id,
                                                         'import_items_index': 0,
                                                         'popups_to_close': 2}}}
                    end_idx = len(self.current_node_items)
                    _add_mnu_but(bool(end_idx), get_txt("current list end"), end_idx)

                    if focused_id and end_idx > 1:
                        idx = self.find_item_index(focused_id)
                        _add_mnu_but(idx != 0, get_txt("before focused item '{focused_id}'"), idx)
                        _add_mnu_but(idx != end_idx - 1, get_txt("after focused '{focused_id}'"), idx + 1)

                else:   # create def_but for error item (moved down to take the above def_but for PyCharm type checking)
                    inf_but = None
                    def_but = {'kwargs': {'text': item['_err_'][:18] + ("..." if len(item['_err_']) > 18 else ""),
                                          'tap_flow_id': id_of_flow('show', 'message'),
                                          'tap_kwargs': {'popup_kwargs': {'title': get_txt("import error(s)"),
                                                                          'message': item['_err_']}},
                                          'square_fill_ink': self.error_ink}}

                if not mnu_maps:    # if first menu item, then add selector menu item with mnu_maps ref to be extended
                    sel_but: dict[str, Any] = deepcopy(def_but)
                    sel_but['cls'] = 'ImportFromSelectButton'
                    sel_kwargs: dict[str, Any] = sel_but['kwargs']
                    sel_kwargs['text'] = item_import_button_text(item, _from)
                    sel_kwargs.pop('square_fill_ink', None)
                    sel_kwargs['ellipse_fill_ink'] = opener.ellipse_fill_ink
                    sel_kwargs['tap_kwargs'].pop('popups_to_close', None)
                    mnu_kwargs = {
                        'opener': opener, 'child_data_maps': mnu_maps, 'auto_width_child_padding': self.font_size * 3}
                    sel_kwargs['on_alt_tap'] = lambda *args: self.change_flow(id_of_flow('open', 'add_menu'),
                                                                              popup_kwargs=mnu_kwargs)
                    sel_maps.append(sel_but)

                # extend dropdown menu
                if inf_but:
                    mnu_maps.append(inf_but)
                mnu_maps.append(def_but)
                mnu_maps.extend(mnu_buts)

        self.vpo(f"LiszApp.show_add_options({opener})")
        focused_id = flow_key(self.flow_id) if flow_action(self.flow_id) == 'focus' else ''
        sel_maps: list[dict] = []

        _add_importable_menus("from clipboard", self.importable_text_node(Clipboard.paste()))
        _add_importable_menus("from file", self.importable_files_nodes(self.documents_root_path))
        _add_importable_menus("from interchange", self.importable_oaios_node())

        if not sel_maps:
            self.show_message(get_txt("neither clipboard nor '{self.documents_root_path}' contains items to import"),
                              title=get_txt("import error(s)", count=1))
            return False

        selector_kwargs = {'opener': opener, 'width': opener.width, 'ellipse_fill_ink': opener.ellipse_fill_ink,
                           'icon_name': id_of_flow('add', 'item'), 'child_data_maps': sel_maps}
        return self.change_flow(id_of_flow('open', 'alt_add'), popup_kwargs=selector_kwargs)

    def show_delete_options(self, opener: Widget) -> bool:
        """ open alternative context menu with actions to delete selected/unselected leaves of the current node.

        :param opener:          widget/FlowButton opening this context menu.
        :return:                True if any delete options are available and got displayed else False.
        """
        def add_delete_options(node_flow_path: List[str], info_text: str):
            """ add del options for leaves of the node specified by node_flow_path (current list/focused sub-list). """
            node = self.flow_path_node(node_flow_path)
            info = self.node_info(node, what=() if self.verbose
                                  else ('selected_leaf_count', 'unselected_leaf_count', 'node_count'))

            if child_maps:              # add separator widget
                child_maps.append({'cls': 'Widget', 'kwargs': {'size_hint_y': None, 'height': self.font_size / 1.8}})

            info['name'] = node_id
            info['content'] = ", ".join((item['id'][:6] + ".." if len(item['id']) > 6 else item['id'])
                                        for item in node[:3]) + ("..." if len(node) > 3 else "")
            child_maps.append({'kwargs': {'text': info_text + " '" + node_id + "'",
                                          'tap_flow_id': id_of_flow('open', 'node_info', repr(node_flow_path)),
                                          'tap_kwargs': {'popup_kwargs': {'node_info': info}},
                                          'image_size': image_size, 'ellipse_fill_ink': self.read_ink,
                                          'ellipse_fill_size': image_size,
                                          'square_fill_ink': self.read_ink[:3] + [0.39]}})

            args_tpl = {'tap_flow_id': id_of_flow('confirm', 'item_deletion', node_id),
                        'tap_kwargs': {'popups_to_close': ('replace_with_data_map_popup',),
                                       'popup_kwargs': {'item_node': node, 'item_ids': (), 'cut_to_clipboard': False}},
                        'icon_name': id_of_flow('delete', 'item'), 'image_size': image_size,
                        'ellipse_fill_ink': self.delete_ink[:3] + [0.69], 'ellipse_fill_size': image_size}

            item_ids: Tuple = tuple(item['id'] for item in node if 'node' in item)
            if item_ids:
                attrs = deepcopy(args_tpl)
                attrs.update(text=get_txt("cut all node items"), icon_name=id_of_flow('cut', 'node'))
                # noinspection PyTypedDict
                attrs['tap_kwargs']['popup_kwargs'] = {'item_node': node, 'item_ids': item_ids,
                                                       'cut_to_clipboard': True}
                child_maps.append({'kwargs': attrs})

                attrs = deepcopy(args_tpl)
                attrs.update(text=get_txt("delete all node items"),
                             square_fill_ink=self.delete_ink[:3] + [0.39])
                # noinspection PyTypedDict
                attrs['tap_kwargs']['popup_kwargs'] = {'item_node': node, 'item_ids': item_ids}
                child_maps.append({'kwargs': attrs})

            item_ids = tuple(item['id'] for item in node if 'node' not in item and item.get('sel') == 1)
            if item_ids:
                attrs = deepcopy(args_tpl)
                attrs.update(text=get_txt("cut {count} selected items", count=len(item_ids)),
                             square_fill_ink=self.update_ink[:3] + [0.39])
                # noinspection PyTypedDict
                attrs['tap_kwargs']['popup_kwargs'] = {'item_node': node, 'item_ids': item_ids,
                                                       'cut_to_clipboard': True}
                child_maps.append({'kwargs': attrs})

                attrs = deepcopy(args_tpl)
                attrs.update(text=get_txt("delete {count} selected items", count=len(item_ids)),
                             square_fill_ink=self.delete_ink[:3] + [0.39])
                # noinspection PyTypedDict
                attrs['tap_kwargs']['popup_kwargs'] = {'item_node': node, 'item_ids': item_ids}
                child_maps.append({'kwargs': attrs})

            item_ids = tuple(item['id'] for item in node if 'node' not in item and item.get('sel') != 1)
            if item_ids:
                attrs = deepcopy(args_tpl)
                attrs.update(text=get_txt("cut {count} unselected items", count=len(item_ids)),
                             square_fill_ink=self.update_ink[:3] + [0.39])
                # noinspection PyTypedDict
                attrs['tap_kwargs']['popup_kwargs'] = {'item_node': node, 'item_ids': item_ids,
                                                       'cut_to_clipboard': True}
                child_maps.append({'kwargs': attrs})

                attrs = deepcopy(args_tpl)
                attrs.update(text=get_txt("delete {count} unselected items", count=len(item_ids)),
                             square_fill_ink=self.delete_ink[:3] + [0.39])
                # noinspection PyTypedDict
                attrs['tap_kwargs']['popup_kwargs'] = {'item_node': node, 'item_ids': item_ids}
                child_maps.append({'kwargs': attrs})

        self.vpo(f"LiszApp.show_delete_options({opener})")
        image_size = (round(self.font_size * 1.35),) * 2
        child_maps: List[dict] = []

        node_id = flow_key(self.flow_path[-1]) if self.flow_path else FLOW_PATH_ROOT_ID
        add_delete_options(self.flow_path, get_txt("current list"))

        focused_id = flow_key(self.flow_id) if flow_action(self.flow_id) == 'focus' else ''
        if focused_id:
            focused_item = self.item_by_id(focused_id)
            if 'node' in focused_item:
                node_id = focused_id
                add_delete_options(self.flow_path + [id_of_flow('enter', 'item', node_id)], get_txt("focused sub-list"))

        if not child_maps:
            self.play_sound('error')
            return False

        popup_kwargs = {'opener': opener, 'child_data_maps': child_maps, 'auto_width_child_padding': self.font_size * 3}
        return self.change_flow(id_of_flow('open', 'alt_delete'), popup_kwargs=popup_kwargs)

    def show_extract_options(self, opener: Widget, flow_path_text: str = '',
                             item: Optional[LiszItem] = None, sel_status: Optional[bool] = None) -> bool:
        """ open context menu with the available extract actions of the current context/flow/app-status (focus, debug).

        :param opener:          opener widget to show extract options for.
        :param flow_path_text:  flow path text to identify the node to extract (def=self.flow_path).
        :param item:            data dict of the item represented by :paramref:`~show_extract_options.widget`.
        :param sel_status:      pass True / False to add option to export selected / unselected items.
        :return:                True if extract options are available and got displayed else False.
        """
        def add_extract_options(node_flow_path: List[str], pre: str = ""):
            """ add extract options for the node specified by node_flow_path.

            :param node_flow_path:  flow path of node to add extract options to the child_maps list.
            :param pre:             info button text prefix (to mark the focused item).
            """
            node = self.flow_path_node(node_flow_path)
            if not node or node_flow_path in added_nodes:
                return      # skip empty and already added nodes
            added_nodes.append(node_flow_path)
            parent_node_id = flow_key(node_flow_path[-1]) if node_flow_path else FLOW_PATH_ROOT_ID

            if child_maps:          # add separator widget
                child_maps.append({'cls': 'Widget', 'kwargs': {'size_hint_y': None, 'height': self.font_size / 1.8}})

            info = self.node_info(node, what=() if self.verbose else ('selected_leaf_count', 'unselected_leaf_count'))
            if parent_node_id:
                info['name'] = parent_node_id

            leaf_count = info['selected_leaf_count'] + info['unselected_leaf_count']
            sel_count = 0 if sel_status is None else \
                (info['selected_leaf_count' if sel_status else 'unselected_leaf_count'])
            info_text = f"{self.flow_path_text(node_flow_path, display_root=True)}" \
                        + (f"   {sel_count}/{leaf_count}" if sel_count else "")
            child_maps.append({'kwargs': {'text': pre + info_text,
                                          'tap_flow_id': id_of_flow('open', 'node_info', repr(node_flow_path)),
                                          'tap_kwargs': {'popup_kwargs': {'node_info': info}},
                                          'image_size': image_size, 'ellipse_fill_ink': self.read_ink,
                                          'ellipse_fill_size': image_size,
                                          'square_fill_ink': self.read_ink[:3] + [0.39]}})

            args_tpl = {'tap_flow_id': id_of_flow('extract', 'node', self.flow_path_text(node_flow_path)),
                        'tap_kwargs': {'popups_to_close': ('replace_with_data_map_popup',), 'extract_type': ''},
                        'icon_name': id_of_flow('export', 'node'), 'image_size': image_size}

            attrs: dict[str, Any] = deepcopy(args_tpl)
            attrs.update(text=get_txt("copy all {count} items", count=leaf_count), icon_name=copy_icon)
            attrs['tap_kwargs']['extract_type'] = 'copy'
            child_maps.append({'kwargs': attrs})

            if self.debug:      # deletion without confirmation - using shrink_node_size() instead of delete_items()
                attrs = deepcopy(args_tpl)
                attrs.update(text=get_txt("cut all {count} items", count=leaf_count),
                             icon_name=id_of_flow('cut', 'node'))
                attrs['tap_kwargs']['extract_type'] = 'cut'
                child_maps.append({'kwargs': attrs})

                attrs = deepcopy(args_tpl)
                attrs.update(text=get_txt("delete all {count} items", count=leaf_count),
                             icon_name=id_of_flow('delete', 'item'),
                             square_fill_ink=self.delete_ink[:3] + [0.39])
                attrs['tap_kwargs']['extract_type'] = 'delete'
                child_maps.append({'kwargs': attrs})

            attrs = deepcopy(args_tpl)
            attrs.update(text=get_txt("export all {count} items", count=leaf_count))
            attrs['tap_kwargs']['extract_type'] = 'export'
            child_maps.append({'kwargs': attrs})

            if is_android:
                attrs = deepcopy(args_tpl)
                attrs.update(text=get_txt("share all {count} items", count=leaf_count))
                attrs['tap_kwargs']['extract_type'] = 'share'
                child_maps.append({'kwargs': attrs})

            if sel_count:
                extract_filter = '_' + ('sel' if sel_status else 'unsel')
                ink = self.selected_ink if sel_status else self.unselected_ink

                attrs = deepcopy(args_tpl)
                attrs.update(
                    text=get_txt("copy {count} {'' if sel_status else 'un'}selected node items", count=sel_count),
                    icon_name=copy_icon, ellipse_fill_ink=ink, ellipse_fill_size=image_size)
                attrs['tap_kwargs']['extract_type'] = 'copy' + extract_filter
                child_maps.append({'kwargs': attrs})

                if self.debug:      # deletion without confirmation - using shrink_node_size() instead of delete_items()
                    attrs = deepcopy(args_tpl)
                    txt = "cut {count} selected items" if sel_status else "cut {count} unselected items"
                    attrs.update(text=get_txt(txt, count=sel_count), icon_name=id_of_flow('cut', 'node'),
                                 ellipse_fill_ink=ink, ellipse_fill_size=image_size)
                    attrs['tap_kwargs']['extract_type'] = 'cut' + extract_filter
                    child_maps.append({'kwargs': attrs})

                    attrs = deepcopy(args_tpl)
                    txt = "delete {count} selected items" if sel_status else "delete {count} unselected items"
                    attrs.update(text=get_txt(txt, count=sel_count), icon_name=id_of_flow('delete', 'item'),
                                 ellipse_fill_ink=ink, ellipse_fill_size=image_size,
                                 square_fill_ink=self.delete_ink[:3] + [0.39])
                    attrs['tap_kwargs']['extract_type'] = 'delete' + extract_filter
                    child_maps.append({'kwargs': attrs})

                attrs = deepcopy(args_tpl)
                attrs.update(
                    text=get_txt("export {count} {'' if sel_status else 'un'}selected node items", count=sel_count),
                    ellipse_fill_ink=ink, ellipse_fill_size=image_size)
                attrs['tap_kwargs']['extract_type'] = 'export' + extract_filter
                child_maps.append({'kwargs': attrs})

                if is_android:
                    attrs = deepcopy(args_tpl)
                    attrs.update(
                        text=get_txt("share {count} {'' if sel_status else 'un'}selected node items", count=sel_count),
                        ellipse_fill_ink=ink, ellipse_fill_size=image_size)
                    attrs['tap_kwargs']['extract_type'] = 'share' + extract_filter
                    child_maps.append({'kwargs': attrs})

        self.vpo(f"LiszApp.show_extract_options({opener}, {flow_path_text}, {item}, {sel_status})")
        flow_path = self.flow_path_from_text(flow_path_text) if flow_path_text else self.flow_path
        has_focus = flow_action(self.flow_id) == 'focus'
        flo_key = flow_key(self.flow_id)
        copy_icon = id_of_flow('copy', 'node')
        image_size = (round(self.font_size * 1.35),) * 2
        child_maps: List[dict] = []
        added_nodes: List[List[str]] = []

        node_id = item['id'] if item else ''
        if node_id and item and 'node' in item:         # added 'and item' from silly mypy
            add_extract_options(flow_path + [id_of_flow('enter', 'item', node_id)],
                                pre=FOCUS_FLOW_PREFIX if has_focus and node_id == flo_key else "")

        add_extract_options(flow_path)

        if has_focus and flo_key != node_id:
            item = self.item_by_id(flo_key)
            if 'node' in item:
                add_extract_options(flow_path + [id_of_flow('enter', 'item', flo_key)], pre=FOCUS_FLOW_PREFIX)

        if not child_maps:
            self.show_message(get_txt("no extract options available for this item/node"))
            return False

        popup_kwargs = {'opener': opener, 'child_data_maps': child_maps, 'auto_width_child_padding': self.font_size * 3}
        return self.change_flow(id_of_flow('open', 'extract'), popup_kwargs=popup_kwargs)

    def widget_by_flow_id(self, flow_id: str) -> Optional[Any]:
        """ determine the widget referenced by the passed flow_id.

        :param flow_id:         flow id referencing the focused widget.
        :return:                widget that has the focus when the passed flow id is set.
        """
        return self.widget_by_id(flow_key(flow_id)) or super().widget_by_flow_id(flow_id)

    def widget_by_id(self, item_id: str) -> Optional[Widget]:
        """ search list item widget """
        nic = cast(Widget, self.items_container)
        for niw in nic.children:
            item_data = getattr(niw, 'item_data', None)
            if item_data and item_data['id'] == item_id:
                return niw
        return None


def share_plain_text(node_text: str, share_name: str):
    """ share node path and content via android SEND/share intent.

    :param node_text:           node text lines (1st line is node flow path and 2nd line node repr string).
    :param share_name:          shortened flow path text of the node to share.
    """
    from jnius import autoclass, cast as jni_cast                                                # type: ignore

    # noinspection PyPep8Naming
    JavaString = autoclass('java.lang.String')                                      # pylint: disable=invalid-name
    # noinspection PyPep8Naming
    Intent = autoclass('android.content.Intent')                                    # pylint: disable=invalid-name
    send_intent = Intent()
    send_intent.setAction(Intent.ACTION_SEND)
    send_intent.setType("text/plain")
    send_intent.putExtra(Intent.EXTRA_TEXT, JavaString(node_text))
    # noinspection PyPep8Naming
    PythonActivity = autoclass('org.kivy.android.PythonActivity')                   # pylint: disable=invalid-name
    current_activity = jni_cast('android.app.Activity', PythonActivity.mActivity)
    chooser = Intent.createChooser(send_intent, jni_cast('java.lang.CharSequence', JavaString(share_name)))
    current_activity.startActivity(chooser)
    print("share_plain_text  node_text=", node_text, "  share_name=", share_name)


if is_android:
    print("LiszApp APP AND SERVICE PREPARE", __name__)


# service and app start
if __name__ in ('__android__', '__main__'):
    if is_android:
        from service import prepare_android_intent_handlers, shared_text_file_path
        print("LiszApp SERVICE START", start_app_service())
    else:
        shared_text_file_path = "new_intent_debug.txt"
    LiszApp(app_name='lisz', app_title="Irmi's Shopping Lisz").run_app()
