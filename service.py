""" android service to receive ACTION_VIEW/share intent.

highly experimental code - don't use in production.
"""
from jnius import autoclass                                                                             # type: ignore
from os import environ
from time import sleep


USE_OLD_TOOLCHAIN_FOR_NEW_INTENT = True         # from android import is done locally

shared_text_file_path = "new_intent_text.txt"

shared_text_receiver_bound = False


def on_new_intent(intent):
    """ android OS intent call back """
    global shared_text_file_path

    print("on_new_intent CALLED", intent)
    print("getAction", intent.getAction())              # "android.intent.action.SEND"
    print("getData", intent.getData())                  # None
    print("getDataString", intent.getDataString())      # None
    print("getExtras", intent.getExtras().toString())   # Bundle[mParcelledData.dataSize=164]
    print("getFlags", intent.getFlags())                # 322961409==0x13400001
    # no attribute getIdentifier exception on: print("getIdentifier", intent.getIdentifier())

    shared_text_content = intent.getStringExtra("android.intent.extra.TEXT")
    print("getStringExtra(android.intent.extra.TEXT)", shared_text_content)

    # file_name = "{ado}/new_intent_text.txt".format(**PATH_PLACEHOLDERS)
    print("SAVE SHARED TEXT INTO", shared_text_file_path)
    with open(shared_text_file_path, 'w') as fp:
        fp.write(shared_text_content)
    print("SHARED TEXT SAVED")


def prepare_android_intent_handlers(called_from: str):
    """ bind intent handlers and prepare reception handlers
    """
    global shared_text_receiver_bound

    print("prepare_android_intent_handlers CALLED FROM", called_from)

    if shared_text_receiver_bound:
        print("prepare_android_intent_handlers cancelled because handler got already bound")
        return

    if USE_OLD_TOOLCHAIN_FOR_NEW_INTENT:
        # OLD TOOLCHAIN - is only working when the main app is running
        # noinspection PyUnresolvedReferences
        from android import activity                                                                    # type: ignore
        activity.bind(on_new_intent=on_new_intent)
    else:
        # NEW TOOLCHAIN - DID NOT GET THIS WORKING
        # adding '.mActivity' to the following autoclass call results in activity==None
        # .. while leaving it away (like now) results in the following exception:
        # .. AttributeError: type object 'org.kivy.android.PythonActivity' has not attribute 'NewIntentListener'
        activity = autoclass('org.kivy.android.PythonActivity')     # .mActivity is None
        print("prepare_android_intent_handlers got activity", activity)
        listener = activity.NewIntentListener(on_new_intent)
        print("                                got listener", listener)
        # activity.mActivity.registerNewIntentListener(listener)
        activity.registerNewIntentListener(listener)
        print("                                registered listener in", activity.newIntentListeners)

    shared_text_receiver_bound = True


if __name__ == '__main__':
    print("SERVICE STARTED WITH ARGUMENT", environ.get('PYTHON_SERVICE_ARGUMENT', ''))
    try:
        prepare_android_intent_handlers("SERVICE RUN")      # fails because mActivity is None / onCreate not called yet
    except (AttributeError, Exception) as ex:
        print("PREPARE ANDROID INTENT HANDLER failed with " + str(ex))
    while True:
        print("SERVICE RUNNING")
        sleep(0.99)
        # maybe better using threading.Timer like shown in p4a/testapps/on_device_unit_tests/test_app/app_service.py
