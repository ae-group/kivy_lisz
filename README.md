# Kivy Lisz

Kivy Lisz is a Python multi-platform application demo project based on the [__Kivy__ Framework](https://kivy.org) 
and the [__ae__ (Application Environment)](https://ae.readthedocs.io "ae on rtd")
namespace package.

The base functionality of this demo application is documented in the
[repository of the ae.lisz_app_data portion](https://ae.readthedocs.io/en/latest/_autosummary/ae.lisz_app_data.html).

On top of the base functionality, the Kivy Lisz app is providing - thanks to Kivy -
also the following extra features:
 
* vibration output support with amplitude configurable by user (only on mobile platforms)



additional credits to:

* [__Erokia__](https://freesound.org/people/Erokia/) and 
  [__plasterbrain__](https://freesound.org/people/plasterbrain/) at
  [freesound.org](https://freesound.org) for the sounds.
* [__iconmonstr__](https://iconmonstr.com/interface/) for the icon images.
