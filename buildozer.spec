# to build on gitlab-ci see https://stackoverflow.com/questions/71632474
# ae:22-03-2023: if version of `sh` (1.14.3) is 2.0 or greater buildozer raises error: str object has no attr stdout
[app]
title = Kivy Lisz
package.name = lisz
package.domain = org.test
source.dir = .

source.exclude_exts = md
source.exclude_dirs = bin, docs, htmlcov, mypy_report, tests
source.exclude_patterns = __pycache__/**, **/__pycache__/**

version.regex = __version__ = ['"](.*)['"]
version.filename = %(source.dir)s/main.py

# ae: pyjnius get added automatically by the kivy requirement
# ae: plyer==master results in buildozer error: No matching distribution found for plyer==master
#requirements = python3,kivy,plyer,ae.core,ae.literal,ae.console,ae.files,ae.updater,ae.gui_app
# V 1.1 12-July-2020: had to add hostpython3 and pinned version to python3 recipes to fix
# .. strange weakref crashes (tried also kivy-20rc3 and other workarounds without success)
# V 1.6.14 28-Oct-2020: switched kivy version from rc3 to 2.0.0rc4.
# V 1.6.16 added oscpy to connect service with main app (compiled ok, but then removed because using temp file)
# V 2.3.49 15-01-2025: changed kivy from 2.1.0 to 2.2.1
# V 2.3.50 15-01-2025: changed kivy from 2.2.1 to 2.3.1 and added ae.cloud_storage, but missed some google-auth packages
# V 2.3.51 15-01-2025: added filetype package needed by kivy 2.3.1 but not to receipt (remove in next kivy version)
# .. plut additional packages needed by google auth api (see also e.g. https://stackoverflow.com/questions/61551856
# .. and https://github.com/Android-for-Python/cloud_storage_examples/tree/main/firebase_admin_example#buildozer;
# .. so finally added - in alphabetic order):
# cachetools, chardet, credentials, google, google-api-core, google-api-python-client, googleapis-common-protos,
# google-auth, google-auth-httplib2, google-auth-oauthlib, gspread, httplib2, idna, oauth2, oauth2client,
# oauthlib, openssl, proto-plus, protobuf, pyasn1, pyasn1-modules, pyopenssl, pyparsing, requests, requests_oauthlib,
# rsa, urllib3, uritemplate
requirements = android, hostpython3==3.9.6, python3==3.9.6, filetype, kivy==2.3.1, plyer,
    pypng, typing_extensions, qrcode, kivy_garden.qrcode,
    cachetools, chardet, credentials, google, google-api-core, google-api-python-client, googleapis-common-protos,
    google-auth, google-auth-httplib2, google-auth-oauthlib, gspread, httplib2, idna, oauth2, oauth2client,
    oauthlib, openssl, proto-plus, protobuf, pyasn1, pyasn1-modules, pyopenssl, pyparsing, requests, requests_oauthlib,
    rsa, urllib3, uritemplate,
    ae.base, ae.files, ae.paths, ae.deep, ae.dynamicod, ae.i18n, ae.updater, ae.core, ae.literal, ae.cloud_storage,
    ae.console, ae.parse_date, ae.gui_app, ae.gui_help, ae.kivy_auto_width, ae.kivy_dyn_chi, ae.kivy_relief_canvas,
    ae.kivy, ae.kivy_glsl, ae.kivy_user_prefs, ae.kivy_file_chooser, ae.kivy_iterable_displayer,
    ae.kivy_qr_displayer, ae.sideloading_server, ae.kivy_sideloading, ae.lisz_app_data, ae.oaio_model, ae.oaio_client

presplash.filename = %(source.dir)s/img/app_icon.jpg
icon.filename = %(source.dir)s/img/app_icon.jpg
orientation = portrait, landscape, portrait-reverse, landscape-reverse

services = Lisz:service.py
fullscreen = 0

android.permissions = INTERNET,READ_EXTERNAL_STORAGE,VIBRATE,WAKE_LOCK,WRITE_EXTERNAL_STORAGE,MANAGE_EXTERNAL_STORAGE
android.accept_sdk_license = True

# (str) Android entry point, default is ok for Kivy-based app
#android.entrypoint = org.kivy.android.PythonActivity

android.manifest.intent_filters = share_intent_filter.xml

#android.archs = armeabi-v7a, arm64-v8a
android.archs = armeabi-v7a

# ae:15-01-2025 added: (bool) enables Android auto backup feature (Android API >=23)
android.allow_backup = True

# ae:08-08-2022: commented out next line to prevent TypeError: cannot pickle '_thread.lock' object
#p4a.branch = develop

# ae:23-03-2022 added next line to ignore setup.py in app projects working tree root; if not only main.py will be copied
# .. but even with this buildozer is still passing --use-setup-py (instead of --ignore-setup-py) to p4a
# so finally removed setup.py from app prj working tree root
#p4a.setup_py = false


[buildozer]
log_level = 2
warn_on_root = 1
